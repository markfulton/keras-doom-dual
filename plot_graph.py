#!/usr/bin/python3

import matplotlib.pyplot as plt
import os
import sys

if __name__ == '__main__':
	plt.style.use('seaborn-whitegrid')
	if len(sys.argv) == 2:
		filepath = str(sys.argv[1])
		print("Plotting: " + str(filepath))
		
		fig = plt.figure()
		ax = plt.axes()
		
		xbuf = []
		ybuf = []
		with open(filepath, "r") as f:
			count = 0
			for line in f:
				try:
					xbuf.append(count)
					ybuf.append(int(line))
					count += 1
				except Exception:
					pass
		
		ax.plot(xbuf, ybuf);
		plt.savefig('testplot.png')
