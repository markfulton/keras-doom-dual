#!/usr/bin/python3

from __future__ import print_function

import skimage
from threading import Lock
import numpy as np
import sys
import os
import datetime
import pickle
import random

from keras import Sequential, callbacks
from keras.layers import Conv2D, BatchNormalization, Activation, Dense, Flatten, Dropout, MaxPooling2D, Convolution2D, TimeDistributed, LSTM
from keras.optimizers import Adam, rmsprop
from multiprocessing import Process, Manager
from skimage import transform, color
from pathlib import Path
import tensorflow
from vizdoom import *


class ReplayMemory():
    def __init__(self, buffer_size=10000):
        self.buffer = []
        self.buffer_size = buffer_size

    def add(self, episode_experience):
        if len(self.buffer) + 1 >= self.buffer_size:
            self.buffer[0:(1+len(self.buffer))-self.buffer_size] = []
        self.buffer.append(episode_experience)

    def sample(self, batch_size, trace_length):
        sampled_episodes = random.sample(self.buffer, batch_size)
        sampledTraces = []
        for episode in sampled_episodes:
            point = np.random.randint(0, len(episode)+1-trace_length)
            sampledTraces.append(episode[point:point+trace_length])
        sampledTraces = np.array(sampledTraces)
        return sampledTraces

class NN:

    def __init__(self, state_size, action_size, learning_rate):
        self.states = Manager().list()
        self.actions = Manager().list()
        self.rewards = Manager().list()
        self.discount_factor = 0.99
        self.train_mode = False
        self.save_model_to_file = False
        self.model_file_name = "deathmatch"
        self.action_size = action_size
        self.state_size = state_size
        self.learning_rate = learning_rate
        #self.model = self.build_reinforce_model(self.state_size, self.action_size, self.learning_rate)
        self.model = self.build_reinforce_model(self.state_size, self.action_size, self.learning_rate)

        self.memory = ReplayMemory()
        self.batch_size = 32
        self.trace_length = 4

    def build_model(self, input_shape, action_size, learning_rate):
        model = Sequential()
        model.add(Convolution2D(32, 8, 8, subsample=(4, 4), input_shape=(input_shape)))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(Convolution2D(64, 4, 4, subsample=(2, 2)))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(Convolution2D(64, 3, 3))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(Flatten())
        model.add(Dense(64))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(Dense(32))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(Dense(output_dim=action_size, activation='sigmoid'))

        adam = Adam(lr=learning_rate)

        model.compile(loss='binary_crossentropy', optimizer=adam)
        return model

    def build_reinforce_model(self, input_shape, action_size, learning_rate):
        model = Sequential()
        model.add(Conv2D(32, (3, 3), input_shape=input_shape))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Conv2D(32, (3, 3), input_shape=input_shape))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Conv2D(64, 3, 3, border_mode='valid'))
        model.add(Activation('relu'))
        model.add(Conv2D(64, 3, 3))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Flatten())

        model.add(Dense(64))
        model.add(BatchNormalization())
        model.add(Activation('relu'))

        model.add(Dense(32))
        model.add(BatchNormalization())
        model.add(Activation('relu'))

        model.add(Dense(output_dim=action_size, activation='softmax'))

        adam = Adam(lr=learning_rate)

        model.compile(loss='categorical_crossentropy', optimizer=adam)

        return model

    def train_on_recordings(self):
        try:
            if self.train_mode:
                in_states = np.array(self.states)
                in_actions = np.array(self.actions)

                if len(in_states) > len(in_actions):
                    print("trimming")
                    in_states = in_states[:len(in_actions)]

                if len(in_actions) > len(in_states):
                    print("trimming")
                    in_actions = in_actions[:len(in_states)]

                loss = self.model.fit(in_states, in_actions, nb_epoch=1, verbose=0)
                self.clear_samples()
                return loss.history['loss']
        except ValueError:
            print("ValueError")
            self.clear_samples()
        return 0

    def train_on_recordings_2(self):
        episode_length = len(self.states)

        discounted_rewards = self.discount_rewards(self.rewards)
        # Standardized discounted rewards
        discounted_rewards -= np.mean(discounted_rewards)
        if np.std(discounted_rewards):
            discounted_rewards /= np.std(discounted_rewards)
        else:
            self.clear_samples()
            print('std = 0!')
            return 0

        update_inputs = np.zeros(((episode_length,) + self.state_size))  # Episode_lengthx64x64x4
        # Similar to one-hot target but the "1" is replaced by discounted_rewards R_t
        advantages = np.zeros((episode_length, self.action_size))

        # Episode length is like the minibatch size in DQN
        for i in range(episode_length):
            update_inputs[i, :, :, :] = self.states[i]
            advantages[i][self.actions[i]] = discounted_rewards[i]

        loss = self.model.fit(update_inputs, advantages, nb_epoch=1, verbose=0)

        self.clear_samples()

        return loss.history['loss']

    def discount_rewards(self, rewards):
        discounted_rewards = np.zeros_like(rewards)
        running_add = 0
        for t in reversed(range(0, len(rewards))):
            if rewards[t] != 0:
                running_add = 0
            running_add = running_add * self.discount_factor + rewards[t]
            discounted_rewards[t] = running_add
        return discounted_rewards

    def predict_next_action(self, state):
        pred = self.model.predict(state).flatten()
        labels = np.zeros(pred.shape)
        labels[pred >= 0.25] = 1.0
        labels[pred < 0.25] = 0.0
        return labels.tolist()

    def predict_next_action_reinforce(self, state):
        policy = self.model.predict(state).flatten()
        return np.random.choice(self.action_size, 1, p=policy)[0], policy

    def process_image(self, img, size):
        img = np.rollaxis(img, 0, 3)
        img = skimage.transform.resize(img, size)
        img = skimage.color.rgb2gray(img)
        return img

    def save_sample(self, screen, action, reward):
        self.states.append(screen)
        self.actions.append(action)
        self.rewards.append(reward)

    def check_valid_sample(self):
        if len(self.states) == len(self.actions):
            return True

        return False

    def clear_samples(self):
        self.states[:] = []
        self.actions[:] = []
        self.rewards[:] = []

    def write_model_to_file(self):
        if self.save_model_to_file:
            if os.path.dirname(self.model_file_name):
                if not os.path.exists(os.path.dirname(self.model_file_name)):
                    os.makedirs(os.path.dirname(self.model_file_name))

            self.model.save_weights(self.model_file_name, overwrite=True)

class Player:

    def __init__(self, is_human, player_num):

        self.current_game = DoomGame()
        #self.current_game.load_config('./scenarios/multi.cfg')
        self.current_game.load_config('./scenarios/defend_the_center.cfg')
        self.is_human = is_human
        self.player_num = player_num
        self.action_size = self.current_game.get_available_buttons_size()

        self.attached_network = None
        self.spawned_process = None
        self.kill_switch = False
        self.train_count = 0
        self.state_size = self.current_game.get_state()

        if self.is_human:
            self.current_game.set_screen_resolution(ScreenResolution.RES_640X480)
            self.current_game.set_window_visible(True)
            self.current_game.set_mode(Mode.SPECTATOR)
        else:
            self.current_game.set_screen_resolution(ScreenResolution.RES_640X480)
            self.current_game.set_window_visible(True)

        #if player_num == 0:
            #self.current_game.add_game_args("-host 2 -deathmatch +timelimit 1 +sv_forcerespawn 1")
        #else:
            #self.current_game.add_game_args("-join 127.0.0.1")

        #self.current_game.add_game_args("+name Player" + str(player_num) + " +colorset 0 -deathmatch")

    def start(self):
        if self.attached_network is None:
            print("Attach a neural network model first!")
            return

        if self.is_human:
            self.spawned_process = Process(target=self.human_loop)
            #self.human_loop()
        else:
            #self.bot_loop() # Predicts don't work in threads
            self.single_bot_loop()
            return

        self.spawned_process.start()

    def attach_network(self, neural_net):
        self.attached_network = neural_net

    def shape_reward(self, prev_ammo, prev_health, prev_kills):

        #last_reward = self.current_game.get_last_reward()
        last_reward = 0
        if prev_health > self.current_game.get_game_variable(GameVariable.HEALTH):
            last_reward = last_reward - 0.1
        if prev_ammo > self.current_game.get_game_variable(GameVariable.SELECTED_WEAPON_AMMO):
            last_reward = last_reward - 0.1
        if self.current_game.get_game_variable(GameVariable.KILLCOUNT) > prev_kills:
            last_reward = last_reward + 1

        return last_reward

    # Screen and actions are recorded to be trained with
    def human_loop(self):
        self.current_game.init()
        count = 0

        self.current_game.set_doom_map("map01")
        self.current_game.set_mode(Mode.ASYNC_PLAYER)
        #self.current_game.send_game_command("removebots")
        #for i in range(5):
            #self.current_game.send_game_command("addbot")

        while not self.kill_switch: # Repeat forever
            self.current_game.new_episode()

            game_state = self.current_game.get_state()
            x_t = game_state.screen_buffer
            x_t = self.attached_network.process_image(x_t, size=(256, 256))
            s_t = np.stack(([x_t] * 4), axis=2)

            while not self.current_game.is_episode_finished():

                game_state = self.current_game.get_state()
                last_action = self.current_game.get_last_action()
                if np.count_nonzero(last_action):

                    if game_state:
                        last_reward = self.current_game.get_last_reward()
                        x_t = game_state.screen_buffer
                        x_t = self.attached_network.process_image(x_t, size=(256, 256))
                        x_t = np.reshape(x_t, (256, 256, 1))
                        s_t = np.append(x_t, s_t[:, :, :3], axis=2)

                        if s_t.shape == (256, 256, 4): # Sometimes screen buffer comes back empty
                            self.attached_network.save_sample(s_t, last_action, last_reward)
                            count += 1
                            with open("./recording/state_output"+str(count)+".bin", "wb") as output_file:
                                pickle.dump(s_t, output_file)
                            #s_t.tofile("./recording/state_output"+str(count)+".bin")
                            with open("./recording/action_output"+str(count)+".bin", "wb") as output_file:
                                pickle.dump(last_action, output_file)
                            #with open("./recording/action_output"+str(count)+".bin", 'w') as f_handle:
                                #np.savetxt(f_handle, last_action)

                self.current_game.advance_action()

    # Predicts actions based on human player samples
    def bot_loop(self):

        self.current_game.init()
        self.train_count = 0

        while not self.kill_switch:
            self.current_game.new_episode()
            game_state = self.current_game.get_state()
            x_t = game_state.screen_buffer
            x_t = self.attached_network.process_image(x_t, size=(256, 256))
            s_t = np.stack(([x_t] * 4), axis=2)
            s_t = np.expand_dims(s_t, axis=0)

            while not self.current_game.is_episode_finished():
                game_state = self.current_game.get_state()
                x_t = game_state.screen_buffer
                x_t = self.attached_network.process_image(x_t, size=(256, 256))
                x_t = np.reshape(x_t, (1, 256, 256, 1))
                s_t = np.append(x_t, s_t[:, :, :, :3], axis=3)

                a_t = np.zeros([self.action_size])
                pred = self.attached_network.predict_next_action(s_t)

                self.current_game.set_action(pred)

                if self.current_game.is_player_dead():
                    self.train_count += 1
                    loss = self.attached_network.train_on_recordings()
                    print("Game: " + str(self.train_count) + " | Frags: " + str(
                        self.current_game.get_game_variable(GameVariable.PLAYER2_FRAGCOUNT)) + " | Loss: " + str(loss))

                    self.attached_network.write_model_to_file()

                self.current_game.advance_action()

    def single_bot_loop(self):
        self.current_game.init()
        self.train_count = 0

        summary_writer = tensorflow.summary.FileWriter("./graph")
        at = tensorflow.Variable(0.0)
        tensorflow.summary.scalar("alive_time", at)
        kls = tensorflow.Variable(0.0)
        tensorflow.summary.scalar("kills", kls)
        write_op = tensorflow.summary.merge_all()
        session = tensorflow.InteractiveSession()
        session.run(tensorflow.global_variables_initializer())

        while not self.kill_switch:
            self.current_game.new_episode()
            alive_time = 0
            game_state = self.current_game.get_state()
            x_t = game_state.screen_buffer
            x_t = self.attached_network.process_image(x_t, size=(256, 256))
            s_t = np.stack(([x_t] * 4), axis=2)
            s_t = np.expand_dims(s_t, axis=0)

            # Init previous stats
            prev_health = self.current_game.get_game_variable(GameVariable.HEALTH)
            prev_kills = self.current_game.get_game_variable(GameVariable.KILLCOUNT)
            prev_ammo = self.current_game.get_game_variable(GameVariable.SELECTED_WEAPON_AMMO)

            while not self.current_game.is_episode_finished():

                alive_time += 1

                game_state = self.current_game.get_state()
                x_t = game_state.screen_buffer
                x_t = self.attached_network.process_image(x_t, size=(256, 256))
                x_t = np.reshape(x_t, (1, 256, 256, 1))
                s_t = np.append(x_t, s_t[:, :, :, :3], axis=3)

                a_t = np.zeros([self.action_size])
                action_idx, policy = self.attached_network.predict_next_action_reinforce(s_t)
                a_t[action_idx] = 1

                a_t = a_t.astype(int)
                self.current_game.set_action(a_t.tolist())

                self.current_game.advance_action(4)

                game_state = self.current_game.get_state()

                last_reward = self.shape_reward(prev_ammo, prev_health, prev_kills)

                prev_health = self.current_game.get_game_variable(GameVariable.HEALTH)
                prev_kills = self.current_game.get_game_variable(GameVariable.KILLCOUNT)
                prev_ammo = self.current_game.get_game_variable(GameVariable.SELECTED_WEAPON_AMMO)

                self.attached_network.save_sample(s_t, action_idx, last_reward)

            self.train_count += 1

            loss = self.attached_network.train_on_recordings_2()

            summary = session.run(write_op, {at: alive_time, kls: prev_kills})
            summary_writer.add_summary(summary, self.train_count)
            summary_writer.flush()

            print("Game: " + str(self.train_count) +
                  " | Kills: " + str(prev_kills) +
                  " | TimeAlive: " + str(alive_time) +
                  " | Loss: " + str(round(loss[0], 5)) + " | SavedAmmo: " + str(prev_ammo))


if __name__ == '__main__':

    print("Starting keras-doom-dual...")

    player_list = []
    #player_list.append(Player(True, len(player_list)))  # Human player
    player_list.append(Player(False, len(player_list)))  # Bot player

    state_size = (256, 256, 4)
    nn = NN(state_size, player_list[0].action_size, 0.0001)

    if len(sys.argv) == 2:
        file_path = str(sys.argv[1])
        model_file = Path(file_path)
        if model_file.exists():

            print("Model found. Loading weights.")
            nn.model.load_weights(model_file)

            text_input = input("Model file '"+file_path+"' weights have been loaded. Enable training mode? (y/n)")

            nn.model_file_name = file_path
            if text_input == "y":
                nn.train_mode = True
                nn.save_model_to_file = True

        else:
            text_input = input("No model of that name found. Create new? (y/n)")

            nn.model_file_name = model_file
            if text_input == "y":
                nn.train_mode = True
                nn.save_model_to_file = True

    else:
        text_input = input("No model loaded. Record training to new file? (y/n)")
        if text_input == "y":
            nn.train_mode = True
            nn.save_model_to_file = True
            nn.model_file_name = "deathmatch_" + str(datetime.datetime.now().strftime('%H_%M_%d_%m_%Y') + ".h5")

    print("SaveModel:" + str(nn.save_model_to_file) + " | TrainMode: " + str(nn.train_mode) + " | ModelFile: " + str(nn.model_file_name))

    # MAKE SURE THIS WORKS ^

    print("Starting players.")
    for player in player_list:
        player.attach_network(nn)
        player.start()

