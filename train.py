#!/usr/bin/python3

from keras import Sequential, Model
from keras.layers import Conv2D, BatchNormalization, Activation, Dense, Flatten, TimeDistributed, LSTM, MaxPooling2D, Dropout, Convolution2D, Input
from keras.optimizers import Adam, rmsprop
import numpy as np
import re
import os
import pickle
from multiprocessing import Process, Manager

def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def unflatten(flat_values):
    shape = (64, 64, 4)
    new_offset = np.product(shape)
    value = flat_values[:new_offset].reshape(shape)
    return value, new_offset


def build_model(input_shape, action_size, learning_rate):
    model = Sequential()
    model.add(Convolution2D(32, 8, 8, subsample=(4, 4), input_shape=(input_shape)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Convolution2D(64, 4, 4, subsample=(2, 2)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Convolution2D(64, 3, 3))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Flatten())
    model.add(Dense(64))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dense(32))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dense(output_dim=action_size, activation='sigmoid'))

    adam = Adam(lr=learning_rate)

    model.compile(loss='binary_crossentropy', optimizer=adam)
    return model

if __name__ == '__main__':

    NN = build_model((64, 64, 4), 9, 0.0001)

    files = os.listdir("./recording")
    files.sort(key=alphanum_key)

    states = Manager().list()
    actions = Manager().list()

    for f in files:
        if f.startswith("state"):
            with open("./recording/"+f, "rb") as output_file:
                try:
                    arr = pickle.load(output_file)
                    states.append(arr)
                except EOFError:
                    pass

        if f.startswith("action"):
            with open("./recording/" + f, "rb") as output_file:
                try:
                    arr = pickle.load(output_file)
                    actions.append(arr)
                except EOFError:
                    pass

    print(len(states))
    print(len(actions))

    NN.fit(np.array(states), np.array(actions), epochs=20)
    NN.save_weights("new_trained_model.h5", overwrite=True)